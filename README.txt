Welcome to the Nodein Kalman Filter test. For this test you will be creating a Kalman filter to keep track of a robot.

For this test the only files you may look at is main.cpp and CMakeList.txt aside from your own. You may use a linear algebra library
of your choosing ,however, I would advise you to use Eigen. You may also use the internet or a textbook fo your choice.

Problem Statement:

A robot is moving along a track. Your task to is to estimate it's position and velocity as it moves along it's path.
You are given it's initial state and periodic measurements of its position as it moves. You can assume that the robot
moves and ouputs measurements at 1 second intervals.

Noise characteristics:
Position = 0.2
Velocity = 0.3
Measurement = 0.1

 

Please fill out the following functions:
- GetInitial(std::vector<float> initial)
- GetMeasurement(std::vector<float> measure)
- std::vector<float> OutputState()


The plotter.py function can be used to see your output compared to the true state.

Your code must compile so edit the CMake file if needed.

Usefull resource:
Example_Kalman.png Your output should look similar to this

Required Libraries:
For plotter.py to run you need python-matplotlib and python-tk
Both can be installed with sudo apt-get
