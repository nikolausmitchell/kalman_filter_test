#ifndef KALMAN_TESTER_H
#define KALMAN_TESTER_H

#include <vector>
#include <random>
#include <stdlib.h>
#include <fstream>
#include <iostream>


class kalman_tester
{
public:
    kalman_tester();
    std::vector<float> true_state;
    std::vector<float> noisy_state;
    std::default_random_engine generator;


    void UpdatePosition(float velocity);
    void RunStep(float velocity);
    std::vector<float> GetNoisyMeasurement();
    std::vector <float> GetTrueState();
    float MeasurePosition();

    void InputAnswer(std::vector<float> answer);

    std::vector<float> initial={0,0};
    std::vector<float> velocities={1,1,2,2,3,-4,-4,-4,-4,1,1,1,0,0,0};
    int num_of_steps;
    int step;
    std::vector<float> GetOneStep();

    std::ofstream file;

    void RunTest();

};

#endif // KALMAN_TESTER_H
