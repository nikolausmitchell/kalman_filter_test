#include <iostream>

#include "kalman_tester.h"
#include <stdlib.h>
#include <fstream>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Core>


using namespace std;
using namespace Eigen;

struct KalmanEstimate{
    MatrixXd x_hat(2,1);
    MatrixXd P_cov(2,2);
};



//Gives the initial state of the system
void GetInitial(std::vector<float> initial, KalmanEstimate KFest){
    //Initializes the estimator

    KFest.x_hat(0,0) = initial(0,0);
    KFest.x_hat(1,0) = initial(1,0);

    KFest.P_cov(0,0) = 1;
    KFest.P_cov(0,1) = 0;
    KFest.P_cov(1,0) = 0;
    KFest.P_cov(1,1) = 1;

};

//Gives a single measurement datapoint
void GetMeasurement(std::vector<float> measure, KalmanEstimate KFest){
    // propagates

    MatrixXd F(2,2); //define F dynamics matrix
    F(0,0) = 1;
    F(0,1) = 1;
    F(1,0) = 0;
    F(1,1) = 1;

    MatrixXd Q(2,2); //define Q noise matrix
    Q(0,0) = 0.2;
    Q(0,1) = 0;
    Q(1,0) = 0;
    Q(1,1) = 0.3;

    //updates

    //HSKR
    MatrixXd H(1,2);
    H(0,0) = 1;
    H(0,1) = 0;




    double R = 0.1;

    MatrixXd Scov = MatrixXd::Zero(2,2);

    MatrixXd K = Matrixxd::Zero(2,2);

    MatrixXd res = MatrixXd::Zero(2,1);

    MatrixXd I = MatrixXd::Identity(2,2);

    //KF Propagate
    KFest.x_hat = F*KFest.x_hat; //propagates the previous estimate as Xnew = F*Xold
    KFest.Pcov = F*KFest.Pcov*F.transpose() + Q; //propagates the covariance estimate

    //KF Update
    res = measure[0] - KFest.x_hat; //computes residual

    Scov = H*KFest.Pcov*H.transpose() + R; //Measurement residual covariance

    K = KFest.Pcov*H.transpose()*Scov.inverse(); //Kalman Gain

    KFest.x_hat = KFest.x_hat + K*res; //Updates the state vector

    KFest.Pcov = (I - K*H)*KFest.Pcov; //Updates Covariance

};

// Please output your state in this function
std::vector<float> OutputState(KalmanEstimate KFest){

    MatrixXd answer = MatrixXd::Zero(2,1);

    answer = KFest.x_hat;

    return answer;


};


int main()
{

    //std::vector<float> answer={0,0};         //DeleteMe (Is Compilecheck only)
    kalman_tester tester;
    KalmanEstimate KFest;

    GetInitial(tester.initial, KFest);

    for(int step=0;step<tester.num_of_steps;step++){
        GetMeasurement(tester.GetOneStep(), KFest);
        //tester.InputAnswer(answer);          //DeleteMe
        tester.InputAnswer(OutputState(KFest)); //UncommentMe
    }
    return 0;
}

