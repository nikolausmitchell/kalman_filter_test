import numpy as np
import matplotlib.pyplot as plt

file_object=open("Datapoints.txt","r")
true_x=[]
true_vx=[]
kalman_x=[]
kalman_vx=[]
for line in file_object:
    token = line.split()
    if token[0] == 'True':
         true_x.append(token[1])
         true_vx.append(token[2])
    elif token[0] == 'Kalman':
        kalman_x.append(token[1])
        kalman_vx.append(token[2])

print len(kalman_x)
t = np.arange(0,15,1)


plt.plot(t,true_x, 'r',label='True X Pos')
plt.plot(t,kalman_x,'g--',label='Kalman X Pos')
plt.plot(t,true_vx,'b',label='True VX')
plt.plot(t,kalman_vx,'c--',label='Kalman VX')
plt.legend(loc='upper right')

plt.legend
plt.show()



