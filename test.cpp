#include <vector>
#include "kalman_filter.h"

kalman_filter filter;

void GetInitial(std::vector<float> initial){
    filter.init(initial);

};

void GetMeasurement(std::vector<float> measure){
    filter.predict();
    filter.Update(measure);

};


std::vector<float> OutputState(){
    std::vector<float> answer;
    answer.resize(2);
    answer[0]=filter.state[0];
    answer[1]=filter.state[1];
    return answer;
};

