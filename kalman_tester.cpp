#include "kalman_tester.h"
#include <random>

kalman_tester::kalman_tester()
{
    true_state.resize(2);
    true_state[0]=0;
    true_state[1]=0;
    std::default_random_engine temp(2);
    generator=temp;
    file.open("Datapoints.txt");
    step=0;
    num_of_steps=velocities.size();

}

void kalman_tester::RunStep(float velocity)
{
    UpdatePosition(velocity);

}

void kalman_tester::UpdatePosition(float velocity)
{
    //For everything the timestep is assumed to be 1s

    true_state[1]=velocity;
    //Position is previous position plus the velocity
    true_state[0]=true_state[0]+true_state[1];
    noisy_state=true_state;

    //Gaussian noise function
    std::normal_distribution<float> x_dist(0,.2);
    std::normal_distribution<float> vx(0,.3);
    //Generate the noise
    float x_noise=x_dist(generator);
    float vx_noise=vx(generator);
    //Add the noise to the true states
    noisy_state[0]+=x_noise;
    noisy_state[1]+=vx_noise;

}

//Return the states with the added noise
std::vector<float> kalman_tester::GetNoisyMeasurement()
{
    return noisy_state;
}

// Measure the position of the robot and assume the sensor has a noise value of .1
float kalman_tester::MeasurePosition(){
    std::normal_distribution<float> x_dist(0,.1);
    return true_state[0]+x_dist(generator);
}

void kalman_tester::InputAnswer(std::vector<float> answer)
{
    std::cout << "Your x Position is " << answer[0] << " and your x velocity is " << answer[1] << std::endl;
    file << "Kalman " << answer[0] << " " << answer[1] << "\n";

}

std::vector<float> kalman_tester::GetOneStep()
{
    RunStep(velocities[step]);
    std::vector<float> state=GetNoisyMeasurement();
    std::vector<float> true_state=GetTrueState();
    file << "True " << true_state[0]<<" " << true_state[1] << "\n";
    step++;
    return state;
}

void kalman_tester::RunTest()
{



}


std::vector<float> kalman_tester::GetTrueState()
{
    return true_state;
}

